const express = require("express");
const fs = require("fs").promises;
const morgan = require("morgan");
const asyncHandler = require("express-async-handler");
const createError = require("http-errors");
const app = express();

app.use(express.json());
app.use(morgan("tiny"));

app.get(
  "/api/files",
  asyncHandler(async (req, res, next) => {
    const files = await fs.readdir("files");
    console.log(files);
    res.status(200).json({
      message: "Success",
      files: files,
    });
  })
);

app.get(
  "/api/files/:file",
  asyncHandler(async (req, res) => {
    const fileName = req.params.file;
    const extension =  /\.[a-z]+/.exec(fileName);
    try {
      const fileContent = await fs.readFile(`./files/${fileName}`, "utf-8");
      res.status(200).json({
        message: "Success",
        filename: fileName,
        content: fileContent,
        extension: extension[0],
         uploadedDate: new Date()
      });
    } catch (error) {
        throw createError(400, `No file with ${fileName} filename found`);
    }
  })
);

app.post(
  "/api/files",
  asyncHandler(async (req, res, next) => {
    const { filename, content } = req.body;

    if (!filename || !filename.match(/.*\.[log|txt|json|yaml|xml|js]/)) {
      throw createError(
        400,
        "Please specify 'filename' with log, txt, json, yaml, xml, js file extensions"
      );
    }

    if (!content) {
      throw createError(400, "Please specify 'content' parameter");
    }

    await fs.writeFile(`./files/${filename}`, content);
    res.status(200).json({
      message: "File created successfully",
    });
  })
);

app.use((req, res, next) => {
    next(createError(400, 'Client error'));
})

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    status: error.status,
    message: error.message,
  });
});

app.listen(8080, () => {
  console.log("Server works at port 8080!");
});
